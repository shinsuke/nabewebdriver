﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NabeWebDriver.Models {
  public class SearchViewModel {
    public SearchViewModel() {
      Results = new List<SearchResultModel>();
    }
    public SearchConditionModel Condition { get; set; }
    public IEnumerable<SearchResultModel> Results { get; set; }
  }
}