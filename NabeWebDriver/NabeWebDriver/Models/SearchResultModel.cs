﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace NabeWebDriver.Models {
  public class SearchResultModel {
    [Display(Name = "ID")]
    public int Id { get; set; }
    [Display(Name = "名前")]
    public string Name { get; set; }
    [Display(Name = "誕生日")]
    [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
    public DateTime Birthday { get; set; }
    [Display(Name = "所持金")]
    [DisplayFormat(DataFormatString = "{0:N0}")]
    public decimal Money { get; set; }
  }
}