﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace NabeWebDriver.Models {
  public class SearchConditionModel {
    [Display(Name = "名前")]
    public string Name { get; set; }
    [Required]
    [Display(Name="誕生日From")]
    [DataType(DataType.Date)]
    [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode=true)]
    public DateTime BirthdayFrom { get; set; }
    [Required]
    [Display(Name="所持金From")]
    [DataType(DataType.Currency)]
    [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
    public decimal? MoneyFrom { get; set; }
  }
}