﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NabeWebDriver.Models;

namespace NabeWebDriver.Controllers {
  public class SearchController : Controller {
    //
    // GET: /Search/

    public ActionResult Index() {
      var model = new SearchViewModel {
        Condition = new SearchConditionModel {
          BirthdayFrom = new DateTime(1950, 1, 1), 
          MoneyFrom = 0
        }
      };
      return View(model);
    }
    public ActionResult Search(SearchConditionModel model) {
      var resultModel = new SearchViewModel() {
        Condition = model
      };
      if (ModelState.IsValid)
      {
        resultModel.Results = new List<SearchResultModel>() {
          new SearchResultModel() { 
            Id = 1, Name = "名前1", Birthday = new DateTime(1970,12,4), Money = 10000m 
          }, 
          new SearchResultModel() { 
            Id = 2, Name = "名前2", Birthday = new DateTime(1980,10,15), Money = 15000m 
          },
          new SearchResultModel() { 
            Id = 3, Name = "名前3", Birthday = new DateTime(1979,6,23), Money = 20000m 
          },
          new SearchResultModel() { 
            Id = 4, Name = "名前4", Birthday = new DateTime(1990,8,6), Money = 25000m 
          },
          new SearchResultModel() { 
            Id = 5, Name = "名前5", Birthday = new DateTime(2000,7,4), Money = 30000m 
          },
        };
      }
      return View("Index", resultModel);
    }
  }
}
